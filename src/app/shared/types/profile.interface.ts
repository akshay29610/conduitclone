export interface ProfileInterface {
  bio: null | any;
  following: boolean;
  image: string;
  username: string;
}
