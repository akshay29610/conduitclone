import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { currentUserSelector, isLoggedInSelector } from 'src/app/auth/store/selector/auth.selector ';
import { CurrentUserInterface } from '../../types/currentUser.interface';

@Component({
  selector: 'cc-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
 isLoggedIn:boolean | null
 currentUser:any | null
  constructor(private store:Store) { }

  ngOnInit(): void {
    this.store.select(isLoggedInSelector).subscribe((posRes)=>{
      this.isLoggedIn =posRes
    })
    this.store.select(currentUserSelector).subscribe((posRes)=>{
      this.currentUser = posRes
    })
  }

}
