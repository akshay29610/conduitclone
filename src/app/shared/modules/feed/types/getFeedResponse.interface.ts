import { ArticleInterface } from "src/app/shared/types/article.interfce";

export interface GetFeedResponseInterface {
  articles: ArticleInterface[]
  articlesCount: number
}
