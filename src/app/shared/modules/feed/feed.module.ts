import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FeedComponent } from './components/feed/feed.component';
import { FeedService } from './services/feed.service';
import { EffectsModule } from '@ngrx/effects';
import { GetFeedEffect } from './store/effects/getFeed.effect';
import { StoreModule } from '@ngrx/store';
import { getFeedReducer } from './store/reducers/getFeed.reducer';
import { MaterialModule } from 'src/app/material/material.module';
import { RouterModule } from '@angular/router';
import { ErrorMessagesModule } from '../error-messages/error-messages.module';
import { LoadingModule } from '../loading/loading.module';
import { PaginationModule } from '../pagination/pagination.module';
import { TagListModule } from '../tag-list/tag-list.module';
import { TogglerModule } from '../toggler/toggler.module';



@NgModule({
  declarations: [
    FeedComponent
  ],
  imports: [
    CommonModule,
    StoreModule.forFeature('feed',getFeedReducer),
    EffectsModule.forFeature([GetFeedEffect]),
    MaterialModule,
    RouterModule,
    ErrorMessagesModule,
    LoadingModule,
    PaginationModule,
    TagListModule,
    TogglerModule
  ],
  exports:[FeedComponent],
  providers:[FeedService]
})
export class FeedModule { }
