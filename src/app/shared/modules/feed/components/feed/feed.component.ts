import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { ArticleInterface } from 'src/app/shared/types/article.interfce';
import { stringify, parseUrl } from 'query-string';
import { environment } from 'src/environments/environment';
import { getFeedAction } from '../../store/action/getFeed.action';
import {
  errorSelector,
  feedSelector,
  isLoadingSelector,
} from '../../store/selectors/selector';
import { GetFeedResponseInterface } from '../../types/getFeedResponse.interface';

@Component({
  selector: 'cc-feed',
  templateUrl: './feed.component.html',
  styleUrls: ['./feed.component.scss'],
})
export class FeedComponent implements OnInit, OnDestroy {
  @Input() apiUrl: string;
  feed: any;
  error: string | null;
  isLoading: boolean;
  limit = environment.limit;
  count: number;
  baseUrl: string;
  currentPage: number;
  queryParamsSubscription: any;
  constructor(
    private store: Store,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.initializeValues();
    this.initializeListners();
  }
  initializeListners(): void {
    this.queryParamsSubscription = this.route.queryParams.subscribe(
      (params: Params) => {
        this.currentPage = Number(params.page || '1');
      }
    );
    this.fetchData();
  }

  initializeValues(): void {
    this.store.select(isLoadingSelector).subscribe((posRes) => {
      this.isLoading = posRes;
    });
    this.store.select(errorSelector).subscribe((posRes) => {
      this.error = posRes;
    });
    this.store
      .select(feedSelector)
      .subscribe((posRes: GetFeedResponseInterface) => {
        this.feed = posRes;
        if (this.feed) {
          this.feed = this.feed.articles;
          this.count = posRes.articlesCount;
        }
      });
    this.baseUrl = this.router.url.split('?')[0]; //It will give all url upto ? symbol
  }
  fetchData(): void {
    const offset = this.currentPage * this.limit - this.limit;
    const parsedUrl = parseUrl(this.apiUrl);
    const stringifiedParams = stringify({
      limit: this.limit,
      offset,
      ...parsedUrl.query,
    });
    const apiUrlWithParams = `${parsedUrl.url}?${stringifiedParams}`;
    this.store.dispatch(getFeedAction({ url: apiUrlWithParams }));
  }
  ngOnDestroy(): void {
    this.queryParamsSubscription.unsubscribe();
  }
}
