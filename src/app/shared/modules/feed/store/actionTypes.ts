export enum ActionTypes{
    GET_FEED = '[feed] Get Feed',
    GET_FEED_SUCCESS = '[feed] Get Feed Success',
    GET_FEED_FAILURE = '[feed] Get Feed Failure'
}
