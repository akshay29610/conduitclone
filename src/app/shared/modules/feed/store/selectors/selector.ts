import { createFeatureSelector, createSelector } from '@ngrx/store';
import { AppStateInterface } from 'src/app/shared/types/appState.interface';
import { FeedStateInterface } from '../../types/feedState.interface';

const getFeedFeatureSelector = createFeatureSelector<
  AppStateInterface,
  FeedStateInterface
>('feed');

export const isLoadingSelector = createSelector(
  getFeedFeatureSelector,
  (state: FeedStateInterface) => {
    return state.isLoading;
  }
);

export const errorSelector = createSelector(
  getFeedFeatureSelector,
  (state: FeedStateInterface) => {
    return state.error;
  }
);

export const feedSelector = createSelector(
  getFeedFeatureSelector,
  (state: FeedStateInterface) => {
    return state.data;
  }
);
