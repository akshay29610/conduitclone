import { routerNavigationAction } from '@ngrx/router-store';
import { Action, createReducer, on } from '@ngrx/store';
import { FeedStateInterface } from '../../types/feedState.interface';
import {
  getFeedAction,
  getFeedFailureAction,
  getFeedSuccesAction,
} from '../action/getFeed.action';

const initialState: FeedStateInterface = {
  data: null,
  isLoading: false,
  error: null,
};

const _getFeedReducer = createReducer(
  initialState,
  on(getFeedAction, (state) => {
    return {
      ...state,
      isLoading: true,
    };
  }),
  on(getFeedSuccesAction, (state, action) => {
    return {
      ...state,
      isLoading: false,
      data: action.feed,
    };
  }),
  on(getFeedFailureAction, (state, action) => {
    return {
      ...state,
    };
  }),
  on(routerNavigationAction, (): FeedStateInterface => initialState)
);

export function getFeedReducer(state: FeedStateInterface, action: Action) {
  return _getFeedReducer(state, action);
}
