import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'cc-tag-list',
  templateUrl: './taglist.component.html',
  styleUrls: ['./taglist.component.scss']
})
export class TaglistComponent implements OnInit {
   @Input() tags:string[]
  constructor() { }

  ngOnInit(): void {
  }

}
