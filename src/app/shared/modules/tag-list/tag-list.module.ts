import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TaglistComponent } from './Components/taglist/taglist.component';
import { MaterialModule } from 'src/app/material/material.module';



@NgModule({
  declarations: [
    TaglistComponent
  ],
  imports: [
    CommonModule,
    MaterialModule
  ],
  exports:[TaglistComponent]
})
export class TagListModule { }
