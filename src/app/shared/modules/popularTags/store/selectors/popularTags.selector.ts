import { createFeatureSelector, createSelector } from "@ngrx/store";
import { AppStateInterface } from "src/app/shared/types/appState.interface";
import { TagStateInterface } from "../../types/tagState.interface";

const popularTagFeatureSelector= createFeatureSelector<AppStateInterface,TagStateInterface>('tags')

export const isPopularTagSelector = createSelector(popularTagFeatureSelector,(state:TagStateInterface)=>{
  return state.data
})

export const isTagLoadingSelector = createSelector(popularTagFeatureSelector,(state:TagStateInterface)=>{
  return state.isTagsLoading
})
export const isErrorSelector = createSelector(popularTagFeatureSelector,(state:TagStateInterface)=>{
  return state.isErrors
})
