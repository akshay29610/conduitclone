import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { catchError, mergeMap } from 'rxjs/operators';
import { map } from 'rxjs/operators';
import { of } from 'rxjs';
import { HttpErrorResponse } from '@angular/common/http';

import { PopularTagsService } from '../../Services/popularTag.service';
import {
  getTagsAction,
  getTagsSuccessAction,
  getTagsFailureAction,
} from '../actions/getTags.action';
import { PopularTagsInterface } from 'src/app/shared/types/popularTags.interface';

@Injectable()
export class GetTagsEffect {
  constructor(
    private getTagService: PopularTagsService,
    private action$: Actions
  ) {}
  getTags$ = createEffect(() => {
    return this.action$.pipe(
      ofType(getTagsAction),
      mergeMap(() => {
        return this.getTagService.getTags().pipe(
          map((tags: PopularTagsInterface) => {
            return getTagsSuccessAction({ tags });
          }),
          catchError(() => {
            return of(getTagsFailureAction());
          })
        );
      })
    );
  });
}
