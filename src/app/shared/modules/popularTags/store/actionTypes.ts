export enum ActionTypes{
  GET_TAGS = '[popular tags] get tags',
  GET_TAGS_SUCCESS = '[popular tags] get tags success',
  GET_TAGS_FAILURE = '[popular tags] get tags failure',
}
