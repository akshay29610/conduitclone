import { createAction, props } from '@ngrx/store';
import { PopularTagsInterface } from 'src/app/shared/types/popularTags.interface';
import { ActionTypes } from '../actionTypes';

export const getTagsAction = createAction(ActionTypes.GET_TAGS);
export const getTagsSuccessAction = createAction(
  ActionTypes.GET_TAGS_SUCCESS,
  props<{ tags: PopularTagsInterface }>()
);
export const getTagsFailureAction = createAction(ActionTypes.GET_TAGS_FAILURE);
