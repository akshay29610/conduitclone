import { Action, createReducer, on } from '@ngrx/store';
import { TagStateInterface } from '../types/tagState.interface';
import {
  getTagsAction,
  getTagsFailureAction,
  getTagsSuccessAction,
} from './actions/getTags.action';
const initialState: TagStateInterface = {
  data: null,
  isErrors: null,
  isTagsLoading: false,
};
const _getTagsReducer = createReducer(
  initialState,
  on(getTagsAction, (state) => {
    return {
      ...state,
      isTagsLoading: true,
    };
  }),
  on(getTagsSuccessAction, (state, action) => {
    return {
      ...state,
      data: action.tags,
    };
  }),
  on(getTagsFailureAction, (state) => {
    return {
      ...state,
      isErrors: 'error while fetching Popular Tags',
    };
  })
);

export function getTagsReducer(state, action: Action) {
  return _getTagsReducer(state, action);
}
