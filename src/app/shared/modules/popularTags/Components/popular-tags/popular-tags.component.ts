import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { PopularTagsInterface } from 'src/app/shared/types/popularTags.interface';
import { getTagsAction } from '../../store/actions/getTags.action';
import {
  isErrorSelector,
  isPopularTagSelector,
  isTagLoadingSelector,
} from '../../store/selectors/popularTags.selector';

@Component({
  selector: 'cc-popular-tags',
  templateUrl: './popular-tags.component.html',
  styleUrls: ['./popular-tags.component.scss'],
})
export class PopularTagsComponent implements OnInit {
  popularTags: PopularTagsInterface | null;
  error: string | null;
  isLoading: boolean;
  constructor(private store: Store) {}

  ngOnInit(): void {
    this.initilizeValues();
  }
  initilizeValues(): void {
    this.store.dispatch(getTagsAction());
    this.store.select(isPopularTagSelector).subscribe((posRes) => {
      this.popularTags = posRes;
    });
    this.store.select(isErrorSelector).subscribe((posRes) => {
      this.error = posRes;
    });
    this.store.select(isTagLoadingSelector).subscribe((posRes) => {
      this.isLoading = posRes;
    });
  }
}
