import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { MaterialModule } from 'src/app/material/material.module';
import { PopularTagsComponent } from './Components/popular-tags/popular-tags.component';
import { PopularTagsService } from './Services/popularTag.service';
import { GetTagsEffect } from './store/effects/getTags.effect';
import { getTagsReducer } from './store/reducers';

@NgModule({
  imports: [
    CommonModule,
    StoreModule.forFeature('tags', getTagsReducer),
    EffectsModule.forFeature([GetTagsEffect]),
    MaterialModule
  ],
  declarations: [PopularTagsComponent],
  exports: [PopularTagsComponent],
  providers: [PopularTagsService],
})
export class PopularTagsModule {}
