import { PopularTagsInterface } from "src/app/shared/types/popularTags.interface";

export interface TagStateInterface{
  data:PopularTagsInterface | null,
  isTagsLoading:boolean,
  isErrors:null |string
}
