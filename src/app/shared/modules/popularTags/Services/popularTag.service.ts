import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { PopularTagsInterface } from 'src/app/shared/types/popularTags.interface';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn:'root'
})
export class PopularTagsService {
  constructor(private http: HttpClient) {}
  getTags(): Observable<PopularTagsInterface> {
    const tagsApi = environment.apiUrl + '/api/tags';
    return this.http.get<PopularTagsInterface>(tagsApi);
  }
}
