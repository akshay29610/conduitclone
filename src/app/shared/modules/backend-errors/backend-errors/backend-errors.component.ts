import { Component, Input, OnInit } from '@angular/core';
import { BackendErrorsInterface } from 'src/app/shared/types/backendError.interface';

@Component({
  selector: 'cc-backend-error-messages',
  templateUrl: './backend-errors.component.html',
  styleUrls: ['./backend-errors.component.scss']
})
export class BackendErrorsComponent implements OnInit {
  @Input() backendErrors
  errorMessages:string[]
  constructor() { }

  ngOnInit(): void {
    this.errorMessages =this.backendErrors
    console.log(this.errorMessages);
  }

}
