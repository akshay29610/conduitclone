import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TogglerComponent } from './Components/toggler/toggler.component';
import { MaterialModule } from 'src/app/material/material.module';



@NgModule({
  declarations: [
    TogglerComponent
  ],
  imports: [
    CommonModule,
    MaterialModule
  ],
  exports:[ TogglerComponent]
})
export class TogglerModule { }
