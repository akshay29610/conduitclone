import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ArticleFormComponent } from './Components/article-form/article-form.component';
import { MaterialModule } from 'src/app/material/material.module';



@NgModule({
  declarations: [
    ArticleFormComponent
  ],
  imports: [
    CommonModule,
    MaterialModule
  ],
  exports:[ArticleFormComponent]
})
export class ArticleFormModule { }
