import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class PersistanceService {
  constructor() {}

  setToken(key: string, data: string): void {
    try {
      localStorage.setItem(key, data);
    } catch (e) {
      console.log(`Error while setting the token: `, e);
    }
  }
  getToken(key: string): string {
    try {
      return localStorage.getItem(key);
    } catch (e) {
      console.log(`Error while retriving token: `, e);
      return '';
    }
  }
}
