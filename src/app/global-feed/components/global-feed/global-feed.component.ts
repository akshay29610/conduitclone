import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'cc-global-feed',
  templateUrl: './global-feed.component.html',
  styleUrls: ['./global-feed.component.scss']
})
export class GlobalFeedComponent implements OnInit {
  apiUrl = '/api/articles'
  constructor() { }

  ngOnInit(): void {
  }

}
