import { createFeatureSelector, createSelector } from '@ngrx/store';
import { AppStateInterface } from 'src/app/shared/types/appState.interface';
import { AuthStateInterface } from '../../types/authState.interface';

const authFeatureSelector = createFeatureSelector<
  AppStateInterface,
  AuthStateInterface
>('auth');

export const isSubmittingSelector = createSelector(
  authFeatureSelector,
  (state: AuthStateInterface) => {
    return state.isSubmitting;
  }
);

export const validationErrorSelector = createSelector(
  authFeatureSelector,
  (state: AuthStateInterface) => {
    return state.validationErrors;
  }
);

export const isLoggedInSelector = createSelector(
  authFeatureSelector,
  (state: AuthStateInterface) => {
    return state.isLoggedIn;
  }
);
export const isAnonymousSelector = createSelector(
  authFeatureSelector,
  (authState: AuthStateInterface) => authState.isLoggedIn === false
);
export const currentUserSelector = createSelector(
  authFeatureSelector,
  (state: AuthStateInterface) => {
    return state.currentUser;
  }
);
