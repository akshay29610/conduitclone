import { createReducer, on } from '@ngrx/store';
import { AuthStateInterface } from '../../types/authState.interface';
import {
  getCurrentUserAction,
  getCurrentUserFailureAction,
  getCurrentUserSuccessAction,
} from '../action/getCurrentUser.action';
import {
  loginAction,
  loginActionFailure,
  loginActionSuccess,
} from '../action/login.action';
import {
  registerAction,
  registerFailureAction,
  registerSuccessAction,
} from '../action/register.action';

const initialState: AuthStateInterface = {
  isSubmitting: false,
  isLoggedIn: null,
  currentUser: null,
  validationErrors: null,
  isLoading: false,
};
const _authReducer = createReducer(
  initialState,
  on(registerAction, (state: AuthStateInterface) => {
    return {
      ...state,
      isSubmitting: true,
    };
  }),
  on(registerSuccessAction, (state, action) => {
    return {
      ...state,
      isLoggedIn: true,
      currentUser: action.currentUser,
    };
  }),
  on(registerFailureAction, (state, action) => {
    return {
      ...state,
      isLoggedIn: false,
      validationErrors: action.errors,
      isSubmitting: false,
    };
  }),
  on(loginAction, (state: AuthStateInterface) => {
    return {
      ...state,
      isSubmitting: true,
      validationErrors: null,
      currentUser: null,
    };
  }),
  on(loginActionSuccess, (state, action) => {
    return {
      ...state,
      isSubmitting: false,
      isLoggedIn: true,
      currentUser: action.currentUser,
      validationErrors: null,
    };
  }),
  on(loginActionFailure, (state, action) => {
    return {
      ...state,
      isLoggedIn: false,
      currentUser: null,
      validationErrors: action.errors,
    };
  }),
  on(
    getCurrentUserAction,
    (state): AuthStateInterface => ({
      ...state,
      isLoading: true,
    })
  ),
  on(
    getCurrentUserSuccessAction,
    (state, action): AuthStateInterface => ({
      ...state,
      isLoading: false,
      isLoggedIn: true,
      currentUser: action.currentUser,
    })
  ),
  on(
    getCurrentUserFailureAction,
    (state): AuthStateInterface => ({
      ...state,
      isLoading: false,
      isLoggedIn: false,
      currentUser: null,
    })
  )
);

export function authReducer(state, action) {
  return _authReducer(state, action);
}
