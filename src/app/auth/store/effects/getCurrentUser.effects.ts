import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { catchError, mergeMap } from 'rxjs/operators';
import { AuthService } from '../../Services/auth.service';
import {
  getCurrentUserAction,
  getCurrentUserSuccessAction,
  getCurrentUserFailureAction,
} from '../action/getCurrentUser.action';
import { PersistanceService } from 'src/app/shared/Services/persistance.service';
import { map } from 'rxjs/operators';
import { of } from 'rxjs';
import { HttpErrorResponse } from '@angular/common/http';
@Injectable()
export class GetCurrentUserEffect {
  constructor(
    private action$: Actions,
    private authService: AuthService,
    private persistanceService: PersistanceService
  ) {}

  getCurrentUser$ = createEffect(() => {
    return this.action$.pipe(
      ofType(getCurrentUserAction),
      mergeMap(() => {
        const token = this.persistanceService.getToken('accessToken');
        if (!token) {
          return of(getCurrentUserFailureAction());
        }
        return this.authService.getCurrentUser().pipe(
          map((currentUser: any) => {
            return getCurrentUserSuccessAction({ currentUser });
          }),
          catchError((err: HttpErrorResponse) => {
            return of(getCurrentUserFailureAction());
          })
        );
      })
    );
  });
}
