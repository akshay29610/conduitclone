import { HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { mergeMap, catchError, map, tap } from 'rxjs/operators';
import { PersistanceService } from 'src/app/shared/Services/persistance.service';
import { CurrentUserInterface } from 'src/app/shared/types/currentUser.interface';
import { AuthService } from '../../Services/auth.service';
import {
  registerAction,
  registerFailureAction,
  registerSuccessAction,
} from '../action/register.action';

@Injectable()
export class RegisterEffects {
  constructor(
    private action$: Actions,
    private authService: AuthService,
    private persistnceService: PersistanceService,
    private router:Router
  ) {}

  register$ = createEffect(() => {
    return this.action$.pipe(
      ofType(registerAction),
      mergeMap(({ request }) => {
        return this.authService.register(request).pipe(
          map((currentUser: any) => {
            this.persistnceService.setToken(
              'accessToken',
              currentUser.user.token
            );
            console.log(`from effects  :`, currentUser.token, currentUser);
            return registerSuccessAction({ currentUser });
          }),
          catchError((err: HttpErrorResponse) => {
            return of(registerFailureAction({ errors: err.error.errors }));
          })
        );
      })
    );
  });

  redirectAfterSubmit$ = createEffect(
    () =>
      this.action$.pipe(
        ofType(registerSuccessAction),
        tap(() => {  // tap is rxjs opertor here is use as normal function when we re not dispatching any action
          this.router.navigateByUrl('/')
        })
      ),
    {dispatch: false} // use with tap operator as tap operator doesn't dispatch any action
  )


}
