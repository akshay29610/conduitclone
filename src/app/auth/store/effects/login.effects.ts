import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { catchError, mergeMap, tap } from 'rxjs/operators';
import { PersistanceService } from 'src/app/shared/Services/persistance.service';
import { AuthService } from '../../Services/auth.service';
import {
  loginAction,
  loginActionFailure,
  loginActionSuccess,
} from '../action/login.action';
import { map } from 'rxjs/operators';
import { of } from 'rxjs';
import { HttpErrorResponse } from '@angular/common/http';
@Injectable()
export class LoginEffects {
  constructor(
    private action$: Actions,
    private authService: AuthService,
    private router: Router,
    private persistanceService: PersistanceService
  ) {}

  login$ = createEffect(() => {
    return this.action$.pipe(
      ofType(loginAction),
      mergeMap(({ request }) => {
        return this.authService.login(request).pipe(
          map((currentUser: any) => {
            this.persistanceService.setToken(
              'accessToken',
              currentUser.user.token
            );
            return loginActionSuccess({ currentUser });
          }),
          catchError((err: HttpErrorResponse) => {
            return of(loginActionFailure({ errors: err.error.errors }));
          })
        );
      })
    );
  });
  redirectAfterSubmit$ = createEffect(
    () =>
      this.action$.pipe(
        ofType(loginActionSuccess),
        tap(() => {
          // tap is rxjs opertor here is use as normal function when we re not dispatching any action
          this.router.navigateByUrl('/');
        })
      ),
    { dispatch: false } // use with tap operator as tap operator doesn't dispatch any action
  );
}
