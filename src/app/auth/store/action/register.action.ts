import { createAction, props } from "@ngrx/store";
import { BackendErrorsInterface } from "src/app/shared/types/backendError.interface";
import { CurrentUserInterface } from "src/app/shared/types/currentUser.interface";
import { ActionType } from "../../types/actionTypes";
import { RegisterRequestInterface } from "../../types/registerRequest.interface";

export const registerAction = createAction(
  ActionType.REGISTER,
  props<{request:RegisterRequestInterface}>()
)

export const registerSuccessAction = createAction(
  ActionType.REGISTER_SUCCESS,
  props<{currentUser:CurrentUserInterface}>()
)

export const registerFailureAction = createAction(
  ActionType.REGISTER_FAILURE,
  props<{errors:BackendErrorsInterface}>()
)
