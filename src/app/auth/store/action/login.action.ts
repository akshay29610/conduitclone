import { createAction, props } from "@ngrx/store";
import { BackendErrorsInterface } from "src/app/shared/types/backendError.interface";
import { CurrentUserInterface } from "src/app/shared/types/currentUser.interface";
import { ActionType } from "../../types/actionTypes";
import { LoginRequestInterface } from "../../types/loginRequest.interface";

export const loginAction = createAction(
  ActionType.LOGIN,
  props<{request:LoginRequestInterface}>()
)

export const loginActionSuccess = createAction(
  ActionType.LOGIN_SUCCESS,
  props<{currentUser:CurrentUserInterface}>()
)

export const loginActionFailure = createAction(
  ActionType.LOGIN_FAILURE,
  props<{errors:BackendErrorsInterface}>()
)
