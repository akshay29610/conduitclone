 import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

import { MaterialModule } from '../material/material.module';
import { RegisterComponent } from './components/register/register.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { StoreModule } from '@ngrx/store';
import { authReducer } from './store/reducer/auth.reducer';
import { EffectsModule } from '@ngrx/effects';
import { RegisterEffects } from './store/effects/register.effects';
import { BackendErrorsModule } from '../shared/modules/backend-errors/backend-errors/backend-errors.module';
import { LoginComponent } from './components/login/login.component';
import { LoginEffects } from './store/effects/login.effects';
import { GetCurrentUserEffect } from './store/effects/getCurrentUser.effects';

const route :Routes = [{
path:'register',component:RegisterComponent
},{
  path:'login',component:LoginComponent
}]
@NgModule({
  declarations: [
    RegisterComponent,
    LoginComponent
  ],
  imports: [
    CommonModule,
    MaterialModule,
    RouterModule.forChild(route),
    ReactiveFormsModule,
    FormsModule,
    StoreModule.forFeature('auth',authReducer),
    EffectsModule.forFeature([RegisterEffects,LoginEffects,GetCurrentUserEffect]),
    BackendErrorsModule
  ]
})
export class AuthModule { }
