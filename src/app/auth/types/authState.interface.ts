import { BackendErrorsInterface } from "src/app/shared/types/backendError.interface";
import { CurrentUserInterface } from "src/app/shared/types/currentUser.interface";

export interface AuthStateInterface{
  isSubmitting:boolean,
  isLoggedIn:boolean | null,
  currentUser:CurrentUserInterface | null,
  validationErrors:BackendErrorsInterface | null,
  isLoading: boolean
}
