import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import { BackendErrorsInterface } from 'src/app/shared/types/backendError.interface';
import { loginAction } from '../../store/action/login.action';
import {
  isLoggedInSelector,
  isSubmittingSelector,
  validationErrorSelector,
} from '../../store/selector/auth.selector ';
import { LoginRequestInterface } from '../../types/loginRequest.interface';
@Component({
  selector: 'cc-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  isSubmitting: boolean;
  backendErrors: BackendErrorsInterface | null;
  isLoggedIn:boolean
  constructor(private _fb: FormBuilder, private store: Store) {}

  ngOnInit(): void {
    this.initializeForm();
    this.initilizeValues();
  }
  initializeForm(): void {
    this.loginForm = this._fb.group({
      email: ['', [Validators.required]],
      password: ['', [Validators.required]],
    });
  }
  initilizeValues(): void {
    this.store.select(isSubmittingSelector).subscribe((posRes) => {
      this.isSubmitting = posRes;
    });
    this.store.select(validationErrorSelector).subscribe((posRes) => {
      this.backendErrors = posRes;
    });
    this.store.select(isLoggedInSelector).subscribe((posRes) => {
      this.isLoggedIn = posRes;
    });
  }
  onSubmit(): void {
    const request: LoginRequestInterface = {
      user: this.loginForm.value,
    };
    this.store.dispatch(loginAction({request}))
  }
}
