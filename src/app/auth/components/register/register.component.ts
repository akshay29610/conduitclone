import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import { BackendErrorsInterface } from 'src/app/shared/types/backendError.interface';
import { AuthService } from '../../Services/auth.service';
import { registerAction } from '../../store/action/register.action';
import { isSubmittingSelector, validationErrorSelector } from '../../store/selector/auth.selector ';
import { AuthStateInterface } from '../../types/authState.interface';
import { RegisterRequestInterface } from '../../types/registerRequest.interface';

@Component({
  selector: 'cc-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  registerForm:FormGroup
  constructor(private _fb:FormBuilder, private store:Store) { }
  isSubmitting:boolean
  backendErrors:BackendErrorsInterface | null
  ngOnInit(): void {
    this.initializeForm()
    this.initializeValues()
  }

  initializeForm():void{
    this.registerForm = this._fb.group({
      username:['',[Validators.required]],
      email:['',[Validators.required]],
      password:['',[Validators.required]]
    })
  }
  onSubmit():void{
    console.log(this.registerForm.value);
    const request: RegisterRequestInterface = {
      user: this.registerForm.value
    }
    this.store.dispatch(registerAction({request}))
  }
  initializeValues():void{
    this.store.select(isSubmittingSelector).subscribe((posRes)=>{
      this.isSubmitting = posRes
    })
    this.store.select(validationErrorSelector).subscribe((posRes)=>{
      this.backendErrors = posRes
    })
  }

}
