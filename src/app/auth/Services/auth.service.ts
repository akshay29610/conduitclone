import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http"
import { RegisterRequestInterface } from '../types/registerRequest.interface';
import { Observable } from 'rxjs';
import { CurrentUserInterface } from 'src/app/shared/types/currentUser.interface';
import { environment } from 'src/environments/environment';
import { LoginRequestInterface } from '../types/loginRequest.interface';
@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http:HttpClient) { }
  URL: string = environment.apiUrl + '/api/users';
  register(data: RegisterRequestInterface): Observable<CurrentUserInterface> {
    return this.http.post<CurrentUserInterface>(this.URL, data);
  }

  login(data:LoginRequestInterface): Observable<CurrentUserInterface> {
    const loginUrl:string = environment.apiUrl + '/api/users/login';
    return this.http.post<CurrentUserInterface>(loginUrl, data);
  }
  getCurrentUser(): Observable<CurrentUserInterface> {
    const url = environment.apiUrl + '/api/user'
    return this.http.get<CurrentUserInterface>(url)
  }
}
